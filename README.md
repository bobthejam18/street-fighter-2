# Street Fighter 2 - JavaScript

Hi there! This repository contains a Street Fighter II game built using fundamental modern front-end technologies. I created it as a study project to deepen my understanding of vanilla JavaScript features and design patterns in large-scale projects, including cleaner utilization of Object Orientation.

I created this with the assistance of Shezzor's Dev Corner's game-making playlist on YouTube.

I hope you find it enjoyable!
